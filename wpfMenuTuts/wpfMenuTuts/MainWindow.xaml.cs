﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfMenuTuts
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void menuOpen_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.ShowDialog();
		}

		private void menuSave_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.ShowDialog();
		}

		private void menuNew_Click(object sender, RoutedEventArgs e)
		{

		}

		private void menuExit_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void menuFontTimes_Click(object sender, RoutedEventArgs e)
		{
			menuFontCourier.IsChecked = false;
			menuFontAriel.IsChecked = false;
			txtBoxDoc.FontFamily = new FontFamily("Times New Roman");
		}

		private void menuFontCourier_Click(object sender, RoutedEventArgs e)
		{
			menuFontTimes.IsChecked = false;
			menuFontAriel.IsChecked = false;
			txtBoxDoc.FontFamily = new FontFamily("Courier");
		}

		private void menuFontAriel_Click(object sender, RoutedEventArgs e)
		{
			menuFontCourier.IsChecked = false;
			menuFontTimes.IsChecked = false;
			txtBoxDoc.FontFamily = new FontFamily("Ariel");
		}
	}
}
